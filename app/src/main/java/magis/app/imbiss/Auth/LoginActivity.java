package magis.app.imbiss.Auth;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import magis.app.imbiss.API.RestAPI;
import magis.app.imbiss.API.User;
import magis.app.imbiss.Config.AppConfig;
import magis.app.imbiss.Config.Easing;
import magis.app.imbiss.Construct.HomeActivity;
import magis.app.imbiss.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mordecai on 1/29/2017.
 */

public class LoginActivity extends AppCompatActivity implements Validator.ValidationListener {

    @NotEmpty
    EditText usernames, passwords;
    ImageView iv_back;
    LinearLayout ll_button, ll_bottom;
    Validator validator;
    SessionManager session;
    SQLiteHandler db;
    Button Login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        validator = new Validator(this);
        validator.setValidationListener(this);

        db = new SQLiteHandler(this);
        session = new SessionManager(this);

        if (session.isLoggedIn())
        {
            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
        }

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        changeStatusBarColor();
        usernames = (EditText) findViewById(R.id.et_username);
        usernames.setPadding(0,15,0,15);
        passwords= (EditText) findViewById(R.id.et_password);
        passwords.setPadding(0,15,0,15);
        ll_button = (LinearLayout) findViewById(R.id.ll_button);
        ll_bottom = (LinearLayout) findViewById(R.id.ll_bottom);
        ease(ll_button);
        ease2(ll_bottom);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, AuthActivity.class));
                finish();
            }
        });
        Login = (Button) findViewById(R.id.btn_sign_up);
        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate(true);
            }
        });
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void ease(final View view) {
        Easing easing = new Easing(1000);
        AnimatorSet animatorSet = new AnimatorSet();
        float fromY = 600;
        float toY = view.getTop();
        ValueAnimator valueAnimatorY = ValueAnimator.ofFloat(fromY,toY);

        valueAnimatorY.setEvaluator(easing);

        valueAnimatorY.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                view.setTranslationY((float) animation.getAnimatedValue());
            }
        });

        animatorSet.playTogether(valueAnimatorY);
        animatorSet.setDuration(700);
        animatorSet.start();
    }

    private void ease2(final View view) {
        Easing easing = new Easing(1200);
        AnimatorSet animatorSet = new AnimatorSet();
        float fromY = 600;
        float toY = view.getTop();
        ValueAnimator valueAnimatorY = ValueAnimator.ofFloat(fromY,toY);

        valueAnimatorY.setEvaluator(easing);

        valueAnimatorY.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                view.setTranslationY((float) animation.getAnimatedValue());
            }
        });

        animatorSet.playTogether(valueAnimatorY);
        animatorSet.setDuration(1100);
        animatorSet.start();
    }

    @Override
    public void onValidationSucceeded() {
        String username = usernames.getText().toString();
        String password = passwords.getText().toString();
        loginUser(username, password);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Fehler!")
                        .setContentText(message)
                        .show();
            }
        }
    }

    public void loginUser(String username, String password) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.ROOT_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        System.out.println("URL : "+retrofit.baseUrl());
        RestAPI request = retrofit.create(RestAPI.class);

        Call<User> user = request.loginUser(username, password);
        user.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User user = response.body();
                System.out.println("Cek Respon : "+user.getUsername());
                System.out.println("Pesan : "+user.getPesan());
                System.out.println("Status : "+user.getStatus());
                int status = Integer.parseInt(user.getStatus());
                String pesan = user.getPesan();

                if (status == 0) {
                    new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Fehler!")
                            .setContentText(pesan)
                            .show();
                } else {
                    final String username = user.getUsername();
                    final String email = user.getEmail();
                    System.out.println("User : "+username+ "\n"
                        + "Email : "+email);

                    final SweetAlertDialog tamvanDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                    tamvanDialog.setTitleText("Success!");
                    tamvanDialog.setContentText(""+pesan);
                    tamvanDialog.setConfirmText("Ok");
                    tamvanDialog.show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            tamvanDialog.dismissWithAnimation();
                            db.addUser(username,email);
                            AkuTamvan();
                        }
                    }, 1500);
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d("TAG_ERROR",""+t.getMessage().toString());
                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!")
                        .setContentText(t.getMessage())
                        .setCancelText("Dismiss")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }

    public void AkuTamvan()
    {
        session.setLogin(true);
        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
        finish();
    }
}
