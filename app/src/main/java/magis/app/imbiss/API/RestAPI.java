package magis.app.imbiss.API;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Mordecai on 1/29/2017.
 */

public interface RestAPI {

    @POST("user/login")
    Call<User> loginUser(
        @Query("username") String username,
        @Query("password") String password
    );

    @POST("user/register")
    Call<User> registerUser(
        @Query("username") String username,
        @Query("email") String email,
        @Query("password") String password
    );
}
