package magis.app.imbiss.API;

/**
 * Created by Mordecai on 1/29/2017.
 */

public class User {

    private String id;
    private String username;
    private String email;
    private String status;
    private String pesan;

    public User(String id, String username, String email, String status, String pesan) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.status = status;
        this.pesan = pesan;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }
}
