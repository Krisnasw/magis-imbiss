package magis.app.imbiss.Fragment;

/**
 * Created by Mordecai on 1/29/2017.
 */

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 *
 */
public class DemoViewPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<LDBFragment> fragments = new ArrayList<>();
    private LDBFragment currentFragment;

    public DemoViewPagerAdapter(FragmentManager fm) {
        super(fm);

        fragments.clear();
        fragments.add(LDBFragment.newInstance(0));
        fragments.add(LDBFragment.newInstance(1));
        fragments.add(LDBFragment.newInstance(2));
        fragments.add(LDBFragment.newInstance(3));
        fragments.add(LDBFragment.newInstance(4));
    }

    @Override
    public LDBFragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (getCurrentFragment() != object) {
            currentFragment = ((LDBFragment) object);
        }
        super.setPrimaryItem(container, position, object);
    }

    /**
     * Get the current fragment
     */
    public LDBFragment getCurrentFragment() {
        return currentFragment;
    }
}
